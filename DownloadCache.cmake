# Download cache utilities for CMake
#
# Copyright (c) 2015, Vladimir Zhirov <vvzh.home@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function(cache_download DOWNLOAD_URL_VAR DOWNLOAD_CACHE_DIR) # Optional argument: СUSTOM_FILE_PREFIX
  if(NOT DOWNLOAD_CACHE_DIR)
    return() # Empty or false DOWNLOAD_CACHE_DIR means caching is disabled, so silently exit
  endif()
  string(REPLACE \\ / DOWNLOAD_CACHE_DIR ${DOWNLOAD_CACHE_DIR})
  if(NOT EXISTS DOWNLOAD_CACHE_DIR)
    file(MAKE_DIRECTORY ${DOWNLOAD_CACHE_DIR})
  endif()
  set(DOWNLOAD_URL ${${DOWNLOAD_URL_VAR}})
  get_filename_component(LOCAL_FILE ${DOWNLOAD_URL} NAME)
  if(ARGV2)
    set(LOCAL_FILE ${ARGV2}${LOCAL_FILE})
  endif()
  set(LOCAL_FILE "${DOWNLOAD_CACHE_DIR}/${LOCAL_FILE}")
  set(LOCAL_URL_FILE "${LOCAL_FILE}.url.txt")
  if(EXISTS ${LOCAL_FILE} AND EXISTS ${LOCAL_URL_FILE})
    file(READ ${LOCAL_URL_FILE} CACHED_FILE_URL)
    if(CACHED_FILE_URL STREQUAL DOWNLOAD_URL)
      message(STATUS "Using file '${LOCAL_FILE}' cached from '${DOWNLOAD_URL}'")
      set(${DOWNLOAD_URL_VAR} ${LOCAL_FILE} PARENT_SCOPE)
      return()
    else()
      message(STATUS "Local file '${LOCAL_FILE}' was cached from different URL '${CACHED_FILE_URL}'. Proceeding with download.")
    endif()
  endif()
  message(STATUS "Caching '${DOWNLOAD_URL}' as '${LOCAL_FILE}'")
  file(DOWNLOAD
    ${DOWNLOAD_URL}
    "${LOCAL_FILE}.tmp"
    SHOW_PROGRESS
    STATUS STATUS_LIST
    LOG LOG_TEXT)
  list(GET STATUS_LIST 0 STATUS_CODE)
  list(GET STATUS_LIST 1 STATUS_TEXT)
  if(NOT STATUS_CODE EQUAL 0)
    # ${LOG_TEXT} may contain a lot of information so it is not included
    # into error message by default. Uncomment the following line if you
    # need to debug failed download:
    # message(STATUS "Problem with download. Log: ${LOG_TEXT}")
    message(FATAL_ERROR
      "Error: failed to download '${DOWNLOAD_URL}', "
      "status code: ${STATUS_CODE}, "
      "status text: ${STATUS_TEXT}")
  endif()
  file(RENAME "${LOCAL_FILE}.tmp" ${LOCAL_FILE})
  file(WRITE ${LOCAL_URL_FILE} ${DOWNLOAD_URL})
  set(${DOWNLOAD_URL_VAR} ${LOCAL_FILE} PARENT_SCOPE)
endfunction()

# Example usage:
# cache_download(ASSIMP_DOWNLOAD_URL "${DOWNLOAD_CACHE_DIR}" "assimp-")
# Quotes around second argument are essential, they help to pass this argument if it is empty.
